# ChangeLog
# By Philip Shen @Gemtek Corp.
# ---------------------------------------------------------------------
# Oct32007 Add "-capture -logdir" for buddy
################################################################################

set currpath [file dirname [pwd]]
set lib_path [file join $currpath "lib"]
source [file join $lib_path "API_Misc.tcl"]
source [file join $lib_path "API_CDRouter.tcl"]

################################################################################
# Start Here
################################################################################
set start_time [ clock seconds ]

if {$argc == 1} {
    set inifname [lindex $argv 0]
    puts "INI file= $inifname"
} else  {
    puts "Please input ini file name."
    return 0
}
set inifile [file join [file dirname [info script]] $inifname]
set csvfile_cgi [file join $lib_path "cgi.csv"]

Func_INI::GetDUTParam $inifile
Func_INI::GetCDRouterParam $inifile

set list_CGIStr  [Func_CSV::Read [list $csvfile_cgi]]

CDRouter::Run $Func_INI::dut_model $Func_INI::dut_fwver $Func_INI::dut_testmode \
        $Func_INI::cdrouter_cofig $Func_INI::cdrouter_testpath \
        $list_CGIStr

set finish_time [ clock seconds ]
Misc::report_duration $start_time $finish_time