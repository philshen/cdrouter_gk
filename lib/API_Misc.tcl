# ChangeLog
# By Philip Shen @SWD Gemtek Corp.
# ---------------------------------------------------------------------
# Sep12-2007 Initialize API_Misc.tcl
# Initialize namespace eval Func_Telnet to control modem
################################################################################

namespace eval Func_INI {
    package require inifile
    
    variable verbose off
    variable logfile "../log/inifile.log"
    variable list_sections {}
    variable list_keys     {}   
    variable list_values    {}
    

 }
proc Func_INI::PntMsg {strmsg} {
    puts $strmsg
}


proc Func_INI::GetSections {inifname} {
    variable list_sections
    
    set hinifile [ini::open $inifname r]
    set list_sections [ini::sections $hinifile]
    ini::close $hinifile
    #list length == 0
    if ![llength $list_sections] {
        return false
    }
    return true
}
proc Func_INI::GetKeys {inifname section} {
    variable list_keys
    
    set hinifile [ini::open $inifname r]
    set list_keys [ini::keys $hinifile $section]
    ini::close $hinifile
    if ![llength $list_keys] {
        return false
    }
    return true
}
proc Func_INI::GetValue {inifname section key} {
    variable list_values
    
    set hinifile [ini::open $inifname r]
    set keyvalue [ini::value $hinifile $section $key]
    ini::close $hinifile
    
    if ![string length $keyvalue] {
        return false
    }
    
    set list_values [split $keyvalue ',']
    return true
}

proc Func_INI::GetSectionsKeys {inifname what} {
    variable list_sections
    variable verbose
    variable logfile
    if {$verbose == on} {
        Log "info" $logfile [list "what=$what"  "Sections=$list_sections"]
    }
    
    foreach temp $list_sections {
        if [string match "*$what" $temp ] {
            GetKeys $inifname $temp
            set section $temp
            return $section
        }
    };#foreach temp $list_sections
    
    return false
}
################################################################################
# proc Func_INI::GetSectionsKeysValue
#
# Return list of value from SectionKeys
################################################################################
proc Func_INI::GetSectionsKeysValue {inifname what} {
    variable list_keys
    variable list_values
    variable verbose
    
    foreach temp $list_keys {
        GetValue $inifname $what $temp
        foreach x $list_values {
            if {$verbose == on} {
                puts "$temp=$x"
            }
            
            #append str_msg $temp "=" $x; lappend list_rtumsg $str_msg
        }
        lappend list_msg $temp $list_values
    };#foreach temp $Func_INI::list_keys
    
    return $list_msg
}

proc Func_INI::Log {level filename list_msg} {
    foreach temp $list_msg {
        set msg "$temp"
        Log::tofile $level $filename $msg
    }
}

proc Func_INI::GetParam_Values {inifname sec_dut} {
    variable verbose
    variable logfile
    
    variable list_keys
    variable list_values
    
    variable dut_testmode
    variable dut_model
    variable dut_fwver
    variable dut_lanip
    variable dut_wanip
    variable dut_keys_values
    variable dut_cgistringid
    variable dut_cgistringid_reset
    variable dut_username_password
    variable dut_wakeuptime
    variable dut_resettime
    variable dut_cgistringid_dmz
    variable dut_cgistringid_port_trigger
    variable dut_cgistringid_url_filter
    variable dut_cgistringid_virtual_server
    variable dut_cgistringid_apps
    variable dut_cgistringid_dyndns
    variable dut_cgistringid_rip
    variable dut_cgistringid_staticroute
    
    variable dut_keys_values_dmz
    
    variable cdrouter_cofig
    variable cdrouter_testpath
    variable cdrouter_ver
    variable cdrouter_lanip
    variable cdrouter_wanip
    # variable cdrouter_testname_id
    variable str_cdrouter_moudle
    
    foreach temp_key [lsort $list_keys] {
        # if {$verbose == on} {
        # puts "temp_key=$temp_key; [llength $temp_key]"
        # }
        
        set retval [GetValue $inifname $sec_dut $temp_key]
        
        switch -regexp [string tolower $temp_key] {
            {^dut_cgistringid_dmz} {
                set dut_cgistringid_dmz  $list_values
            }
            {^dut_cgistringid_porttrigger} {
                set dut_cgistringid_port_trigger  $list_values
            }
            {^dut_cgistringid_urlfilter} {
                set dut_cgistringid_url_filter  $list_values
            }
            {^dut_cgistringid_virtualserver} {
                set dut_cgistringid_virtual_server  $list_values
            }
            {^dut_cgistringid_apps} {
                set dut_cgistringid_apps  $list_values
            }
            {^dut_cgistringid_dyndns} {
                set dut_cgistringid_dyndns  $list_values
            }
            {^dut_cgistringid_rip} {
                set dut_cgistringid_rip  $list_values
            }
            {^dut_cgistringid_staticroute} {
                set dut_cgistringid_staticroute  $list_values
            }
            {^dut_keys_values_dmz} {
                set dut_keys_values_dmz $list_values
            }
            {^dut_test_mode$} {
                set dut_testmode $list_values
            }
            {^dut_model$} {
                set dut_model $list_values
            }
            {^dut_fwver} {
                set dut_fwver $list_values
            }
            {^dut_lan_ip} {
                set dut_lanip $list_values
            }
            {^dut_wan_ip} {
                set dut_wanip $list_values
            }
            {^dut_keys_values} {
                set dut_keys_values $list_values
            }
            {^dut_cgistringid$} {
                set dut_cgistringid $list_values
            }
            {^dut_cgistringid_reset$} {
                set dut_cgistringid_reset $list_values
            }
            {^dut_username_password$} {
                set dut_username_password $list_values
            }
            {^dut_wakeupduration$} {
                set dut_wakeuptime $list_values
            }
            {^dut_resetduration$} {
                set dut_resettime $list_values
            }
            {^cdrouter_ver} {
                set cdrouter_ver $list_values
            }
            {^cdrouter_lan_ip} {
                set cdrouter_lanip $list_values
            }
            {^cdrouter_wan_ip} {
                set cdrouter_wanip $list_values
            }
            {^cdrouter_testname_id[0-1][0-9]$} {
                # puts "temp_key=$temp_key; [llength $temp_key]"
                Func_INI::GetCDRouterParam_str_TestModules $list_values
                
            }
            {^cdrouter_cofig$} {
                set cdrouter_cofig $list_values
            }
            {^cdrouter_testpath$} {
                set cdrouter_testpath $list_values
            }
            
        };#switch -regexp [string tolower $temp]
        
        if {$verbose == on} {
            puts "temp_key=$temp_key; list_values=$list_values"
            
        }
    };#foreach temp_key $list_keys
    
    
    return true
}
proc Func_INI::GetDUTParam {inifname} {
    variable verbose
    variable logfile
    variable list_sections
    variable list_keys
    
    set retval [GetSections $inifname]
    if {$retval == false} {
        return false
    }
    
    if {$verbose == on} {
        puts "list_sections=$list_sections; [llength $list_sections]"
    }
    
    foreach temp $list_sections {
        switch -regexp $temp {
            {^Param_DUT} {
                set sec_dut $temp
                
                set retval [GetKeys $inifname $sec_dut]
                if {$retval == false} {
                    return false
                };#if {$retval == false}
                
                if {$verbose == on} {
                    puts "list_keys=$list_keys; [llength $list_keys]"
                }
                
                set retval [Func_INI::GetParam_Values $inifname $sec_dut]
                if {$retval == false} {
                    return false
                };#if {$retval == false}
            }
        };#switch -regexp $temp
    };#foreach temp $list_sections
    
    return true
}
proc Func_INI::GetCDRouterParam_TestModules {} {
    variable str_cdrouter_moudle
    variable cdrouter_testname_id
    
    foreach {temp} [split $str_cdrouter_moudle ";"] {
        lappend cdrouter_testname_id $temp
    }
    
    set cdrouter_testname_id [list $cdrouter_testname_id]
}
proc Func_INI::GetCDRouterParam_str_TestModules {list_values} {
    variable verbose
    variable str_cdrouter_moudle
    
    if [info exist temp_arr_cdrouter_moudle] {
        unset temp_arr_cdrouter_moudle
    }
    foreach temp $list_values {
        foreach cdrouter_testmodule $temp {
            append temp_str_cdrouter_moudle $cdrouter_testmodule ";"
            if {$verbose == "on"} {
                puts "In proc Func_INI::GetCDRouterParam_TestModules."
                puts "$cdrouter_testmodule, $temp_str_cdrouter_moudle"
            };#if {$verbose == "on"}
        }
    }
    
    append str_cdrouter_moudle $temp_str_cdrouter_moudle
    
}
proc Func_INI::GetCDRouterParam {inifname} {
    variable verbose
    variable logfile
    variable list_sections
    variable list_keys
    
    set retval [GetSections $inifname]
    if {$retval == false} {
        return false
    }
    
    if {$verbose == on} {
        puts "list_sections=$list_sections; [llength $list_sections]"
    }
    
    foreach temp $list_sections {
        switch -regexp $temp {
            {^Param_CDRouter} {
                set sec_dut $temp
                
                set retval [GetKeys $inifname $sec_dut]
                if {$retval == false} {
                    return false
                };#if {$retval == false}
                
                if {$verbose == on} {
                    puts "list_keys=$list_keys; [llength $list_keys]"
                }
                
                set retval [Func_INI::GetParam_Values $inifname $sec_dut]
                if {$retval == false} {
                    return false
                };#if {$retval == false}
            }
        };#switch -regexp $temp
    };#foreach temp $list_sections
    
    ##Get cdrouter_testmoudles form ID00,ID01,... in INI file by list
    Func_INI::GetCDRouterParam_TestModules
    
    return true
}
########################################
# Define a simple custom logproc
##########################################
namespace eval Log {
    package require logger
}

# Define a simple custom logproc
proc Log::log_to_file {lvl logfiletxt} {
    # set logfile "mylog.log"
    set msg "\[[clock format [clock seconds] -format "%H:%M:%S %m%d%Y"]\] \
            [lindex $logfiletxt 1]"
    set f [open [lindex $logfiletxt 0] {WRONLY CREAT APPEND}] ;# instead of "a"
    fconfigure $f -encoding utf-8
    puts $f $msg
    close $f
}
################################################################################
#${log}::logproc level
# ${log}::logproc level command
# ${log}::logproc level argname body
# This command comes in three forms - the third, older one is deprecated
# and may be removed from future versions of the logger package.
# The current set version takes one argument, a command to be executed
# when the level is called. The callback command takes on argument,
# the text to be logged. If called only with a valid level logproc returns
# the name of the command currently registered as callback command.
# logproc specifies which command will perform the actual logging for a given level.
# The logger package ships with default commands for all log levels,
# but with logproc it is possible to replace them with custom code.
# This would let you send your logs over the network, to a database, or anything else.
################################################################################
proc Log::tofile {level strfilename strtext} {
    # Initialize the logger
    set log [logger::init global]
    
    # Install the logproc for all levels
    foreach lvl [logger::levels] {
        interp alias {} log_to_file_$lvl {} Log::log_to_file $lvl
        ${log}::logproc $lvl log_to_file_$lvl
    }
    
    ${log}::$level [list $strfilename $strtext]
    
    return true
}

proc Log::parray {a {pattern *}} {
    upvar 1 $a array
    if ![array exists array] {
        error "\"$a\" isn't an array"
    }
    set maxl 0
    foreach name [lsort [array names array $pattern]] {
        if {[string length $name] > $maxl} {
            set maxl [string length $name]
        }
    }
    set maxl [expr {$maxl + [string length $a] + 2}]
    foreach name [lsort [array names array $pattern]] {
        set nameString [format %s(%s) $a $name]
        puts stdout [format "%-*s = %s" $maxl $nameString $array($name)]
    }
}
proc Log::LogList_level {level filename list_msg} {
    set path_logfile [file join ".." "log" $filename]
    
    foreach temp $list_msg {
        set strmsg "$temp"
        Log::tofile $level $path_logfile $strmsg
    }
}
proc Log::LogOut {str_out logfname} {
    
    set path_logfile [file join ".." "log" $logfname]
    # set clk [clock format [clock seconds] -format "%b%d%H%M%S%Y"]
    # append str_out;# "  ";append Str_Out $clk
    
    set rnewinfd [open $path_logfile a]
    puts $rnewinfd $str_out
    flush $rnewinfd;close $rnewinfd
}

namespace eval AP_Router {
    package require TclCurl
    package require struct
    
    variable verbose off
    variable logfile "AP_Router.log"
    variable list_pair_key_value {}
    variable list_pair_key_value_updated {}
    variable str_cgi_keyvalue ""
}
proc AP_Router::CheckCgiStr {cgistr} {
    set retval 0
    set len_cgistr [string length $cgistr]
    
    if {$len_cgistr == 0} {
        set retval 1
        return $retval
    }
    
    return $retval
}
proc AP_Router::GetListPair_KeyValue {cgistr} {
    variable verbose
    variable logfile
    variable list_pair_key_value
    set retval 0
    
    ##check cgistr if available
    set retval [AP_Router::CheckCgiStr $cgistr]
    if {$retval == 1} {
        return $retval
    }
    ##split char '&'
    set list_cgistr [split $cgistr &]
    
    if {$verbose == "on"} {
        set strlogmsg "In proc AP_Router::GetListPair_KeyValue"
        Func_INI::PntMsg $strlogmsg
        Log::LogOut $strlogmsg $logfile
    }
    
    ################################################################################
    # For proc AP_Router::GetUpdatedCGIString_List, reset variable list_pair_key_value
    ################################################################################
    if {[llength $list_pair_key_value] > 0} {
        set list_pair_key_value {}
    }
    
    set num 0
    foreach {temp} $list_cgistr {
        # puts $temp
        incr num
        
        ##split char '='
        foreach {key value} [split $temp =] {
            if {$verbose == "on"} {
                set strlogmsg "pair$num: key=$key; value=[curl::unescape $value]"
                Func_INI::PntMsg $strlogmsg
                Log::LogOut $strlogmsg $logfile
            }
            
            set list_key_value [list $key $value]
        };#foreach {key value} [split $temp =]
        
        lappend list_pair_key_value $list_key_value
    };#foreach {temp} $list_cgistr
    
    return true
}

proc AP_Router::Relpace_KeyValue_Token {key value list_token_value} {
    variable verbose
    set list_keyvalue {}
    
    ##set list_token_value {{pppoe_username ab_cd} {pppoe_password cd_ab}}
    foreach temp $list_token_value {
        
        for {set i 0} {$i < [llength $temp]} {incr i} {
            set element [lindex $temp $i]
            # puts "element=$element; [llength $element]"
            ##element {pppoe_username ab_cd} {pppoe_password cd_ab}
            
            foreach {token_key token_value} $element {
                
                if {$token_key == $key} {
                    set list_keyvalue [list $key $token_value]
                    
                    if {$verbose == "on"} {
                        set strlogmsg "In proc AP_Router::Relpace_KeyValue_Token"
                        Func_INI::PntMsg $strlogmsg
                        set strlogmsg "pair_orginal: key=$key; value=$value"
                        Func_INI::PntMsg $strlogmsg
                        # set strlogmsg "temp= $temp; [llength $temp]"
                        # Func_INI::PntMsg $strlogmsg
                        # puts "token_key= $token_key ;token_value= $token_value"
                        set strlogmsg "list_keyvalue: $list_keyvalue"
                        Func_INI::PntMsg $strlogmsg
                    };#if {$verbose == "on"}
                    return $list_keyvalue
                };#if {$token_key == $key}
                
            };#foreach {token_key token_value} $element
            
        };#for {set i 0} {$i < [llength $temp]} {incr i}
            
    };#foreach temp $list_token_value
    
    return $list_keyvalue
}

proc AP_Router::GetListPair_KeyValue_Updated {list_token_value} {
    variable verbose
    variable logfile
    #{ {command device_data} {cur_ipaddr 192.168.1.1}...... }
    variable list_pair_key_value
    variable list_pair_key_value_updated
    set retval 0
    
    ################################################################################
    # For proc AP_Router::GetUpdatedCGIString_List, reset variable list_pair_key_value_updated
    ################################################################################
    if {[llength $list_pair_key_value_updated] > 0} {
        set list_pair_key_value_updated {}
    }
    
    foreach list_temp $list_pair_key_value {
        
        foreach {key value} $list_temp {
            
            set list_key_value \
                    [AP_Router::Relpace_KeyValue_Token $key $value $list_token_value]
            
        };#foreach {key value} $list_temp
        
        if {[llength $list_key_value] == 0} {
            set list_key_value [list $key $value]
        }
        lappend list_pair_key_value_updated $list_key_value
        
        foreach temp $list_token_value {
            set token_key [lindex $temp 0]
            
            if {$key == $token_key} {
                if {$verbose == "on"} {
                    set strlogmsg "pair_orginal: key=$key; value=$value"
                    Func_INI::PntMsg $strlogmsg
                    set strlogmsg "pair_updated: $list_key_value"
                    Func_INI::PntMsg $strlogmsg
                };#if {$verbose == "on"}
            }
            
        };#foreach temp $list_token_value
        
    };#foreach list_temp $list_pair_key_value
    
    if {$verbose == "on"} {
        set strlogmsg "In proc AP_Router::GetListPair_KeyValue_Updated"
        Func_INI::PntMsg $strlogmsg
        Func_INI::PntMsg "list_pair_key_value= $list_pair_key_value"
        Func_INI::PntMsg "list_pair_key_value_updated= $list_pair_key_value_updated"
    };#if {$verbose == "on"}
    
    return $retval
}
proc AP_Router::GetDUTURL_GET {lan_ip list_username_passwd} {
    foreach {temp} $list_username_passwd {
        foreach {element} $temp {
            foreach {u p} $element {
                # puts "$u";# puts "$p"
                append str_url "http:\/\/" "$u" ":" "$p" "@" "$lan_ip"
            }
        }
    };#foreach {temp} $list_username_passwd
    
    return $str_url
}
proc AP_Router::GetDUTURL_POST_List {lan_ip list_username_passwd list_CGIStr list_cgistr_id} {
    # puts $lan_ip,$list_username_passwd ;#GetDUTURL
    set list_target [Func_CSV::GetPostTarget $list_CGIStr $list_cgistr_id]
    # puts $list_CGIStr,$list_cgistr_id
    # puts "list_target= $list_target"
    
    foreach {temp} $list_username_passwd {
        foreach {element} $temp {
            foreach {u p} $element {
                # puts "$u";# puts "$p"
                foreach {target} $list_target {
                    set str_url ""
                    append str_url "http:\/\/" "$u" ":" "$p" \
                            "@" "$lan_ip" "\/" $target
                    lappend list_str_url $str_url
                }
                
            }
        }
    };#foreach {temp} $list_username_passwd
    
    return $list_str_url
    
}
proc AP_Router::GetDUTURL_POST {lan_ip list_username_passwd list_CGIStr list_cgistr_id} {
    set target [Func_CSV::GetPostTarget $list_CGIStr $list_cgistr_id]
    # puts $target
    
    foreach {temp} $list_username_passwd {
        foreach {element} $temp {
            foreach {u p} $element {
                # puts "$u";# puts "$p"
                append str_url "http:\/\/" "$u" ":" "$p" \
                        "@" "$lan_ip" "\/" $target
            }
        }
    };#foreach {temp} $list_username_passwd
    
    return $str_url
}

proc AP_Router::GetStr_CGI {list_token_value} {
    variable verbose
    variable logfile
    variable list_pair_key_value_updated
    variable str_cgi_keyvalue
    set retval 0
    
    AP_Router::GetListPair_KeyValue_Updated $list_token_value
    
    ################################################################################
    # For proc AP_Router::GetUpdatedCGIString_List, reset variable str_cgi_keyvalue
    ################################################################################
    if {[llength $str_cgi_keyvalue] > 0} {
        set str_cgi_keyvalue ""
    }
    
    set list_pairs {}
    set num 0
    foreach temp $list_pair_key_value_updated {
        foreach {name value} $temp {
            #check value length
            if {[llength $value] == 0} {
                set pairs "$name="
            } else  {
                
                switch -regexp [string tolower $name] {
                    {^timezone} {
                        set pairs "$name=[curl::escape $value]"
                    }
                    default {
                        set pairs "$name=$value"
                    }
                };#switch -regexp [sting tolower $name]
                
            };#if {[llength $value] == 0}
            
        };#foreach {name value} $temp
        
        lappend list_pairs $pairs
    }
    append str_cgi_keyvalue [join $list_pairs &]
    
    if {$verbose == "on"} {
        set strlogmsg "In proc AP_Router::GetStr_CGI"
        Func_INI::PntMsg $strlogmsg
        Log::LogOut $strlogmsg $logfile
        set strlogmsg "list_pair_key_value_updated= $list_pair_key_value_updated"
        Func_INI::PntMsg $strlogmsg
        Log::LogList_level "info" $logfile $strlogmsg
        set strlogmsg "str_cgi_keyvalue= $str_cgi_keyvalue"
        Func_INI::PntMsg $strlogmsg
        Log::LogList_level "info" $logfile $strlogmsg
    }
    return $retval
}
proc AP_Router::GetUpdatedCGIString_List {list_cgistringid list_CGIStr {list_token_value {}}} {
    variable verbose
    
    foreach {cgistr_id} $list_cgistringid {
        set retval [Func_CSV::GetTargetCGIString $list_CGIStr $cgistr_id]
        set cgistr $Func_CSV::targetid_cgi_str
        
        # puts "CGIStr= $cgistr"
        if {[string length $cgistr] ne 0} {
            
            set retval [AP_Router::GetListPair_KeyValue $cgistr]
            
            # AP_Router::GetListPair_KeyValue_Updated $list_token_value
            AP_Router::GetStr_CGI $list_token_value
            
            if {$verbose == "on"} {
                puts "In proc AP_Router::GetUpdatedCGIString_List"
                puts "cgistr_id= $cgistr_id"
                puts "$AP_Router::list_pair_key_value"
                puts "Len of list_pair_key_value= [llength $AP_Router::list_pair_key_value]"
                
                set AP_Router::logfile "cgi_str_update.txt"
                ##diff AP_Router::list_pair_key_value and AP_Router::list_pair_key_value_updated
                AP_Router::DiffList $AP_Router::list_pair_key_value \
                        $AP_Router::list_pair_key_value_updated
                        
                puts "AP_Router::str_cgi_keyvalue= $AP_Router::str_cgi_keyvalue"
                puts "[llength $AP_Router::str_cgi_keyvalue]"
                
            };#if {$verbose == "on"}
            
            lappend list_str_cgi_keyvalue $AP_Router::str_cgi_keyvalue
        }
    }
    return $list_str_cgi_keyvalue
}
proc AP_Router::GetUpdatedCGIString {list_cgistringid list_CGIStr} {
    foreach {cgistr_id} $list_cgistringid {
        set retval [Func_CSV::GetTargetCGIString $list_CGIStr $cgistr_id]
        set cgistr $Func_CSV::targetid_cgi_str
        # puts "CGIStr= $cgistr"
        if {[string length $cgistr] ne 0} {
            
            set retval [AP_Router::GetListPair_KeyValue $cgistr]
            
            # set AP_Router::verbose "on"
            set AP_Router::logfile "cgi_str_update.txt"
            
            set list_token_value $Func_INI::dut_keys_values
            # AP_Router::GetListPair_KeyValue_Updated $list_token_value
            AP_Router::GetStr_CGI $list_token_value
            
            ##diff AP_Router::list_pair_key_value and AP_Router::list_pair_key_value_updated
            AP_Router::DiffList $AP_Router::list_pair_key_value \
                    $AP_Router::list_pair_key_value_updated
            # puts "$AP_Router::str_cgi_keyvalue"
        }
    };#foreach {cgistr_id} $list_cgistringid
}
proc AP_Router::HttpPost_Auth_Reset {list_cgistringid list_CGIStr} {
    #Factor Reset CGI sting
    AP_Router::GetUpdatedCGIString $list_cgistringid $list_CGIStr
    set url [AP_Router::GetDUTURL_POST $Func_INI::dut_lanip $Func_INI::dut_username_password \
            $list_CGIStr $Func_INI::dut_cgistringid_reset]
    AP_Router::HttpPost_Auth $url $AP_Router::str_cgi_keyvalue
}
proc ProgressCallback {dltotal dlnow ultotal ulnow} {
    
    set dltotal [expr int($dltotal)]
    set dlnow   [expr int($dlnow)]
    set ultotal [expr int($ultotal)]
    set ulnow   [expr int($ulnow)]
    # set txt "========================================================="
    # Misc::insertLogLine "info" $txt Progress
    set txt "callback: dltotal=$dltotal - dlnow=$dlnow - ultotal=$ultotal - ulnow=$ulnow"
    Misc::insertLogLine "info" $txt
    # set txt "========================================================="
    # Misc::insertLogLine "info" $txt
    # set txt "!!!Waiting 20 seconds!!!"
    # Misc::insertLogLine "info" $txt
    # after [expr 20 * 1000];update
    return
}
proc AP_Router::HttpPost_Auth {url data} {
    set curlHandle [curl::init]
    $curlHandle reset
    
    $curlHandle configure -url $url -post 1 -encoding deflated \
            -useragent "Mozilla 4.0 (compatible; Getleft 0.10.4)" \
            -postfields $data -verbose 0 -connecttimeout 1 \
            -noprogress 0 ;#-progressproc ProgressCallback -timeout 3
    
    
    catch { $curlHandle perform } curlErrorNumber
    # if { $curlErrorNumber != 0 } {
        # error [curl::easystrerror $curlErrorNumber]
    # }
    
    set Http_code [$curlHandle getinfo responsecode]
    $curlHandle cleanup
    
    return $Http_code
}
proc AP_Router::HttpGet_Chk {dut_lanip dut_usernamepasswd} {
    set curlHandle [curl::init]
    set url [AP_Router::GetDUTURL_GET $dut_lanip $dut_usernamepasswd]
    $curlHandle configure -url $url -httpget 1  -verbose 0;# \
            $curlHandle perform
    set Http_code [$curlHandle getinfo responsecode]
    
    $curlHandle cleanup
    return $Http_code
}
proc AP_Router::DUT_WAN_Setting {list_cgistringid list_CGIStr} {
    set txt "!!!DUT Reset!!!"
    Misc::insertLogLine "info" $txt
    
    #Factor Reset CGI setting
    AP_Router::HttpPost_Auth_Reset $Func_INI::dut_cgistringid_reset $list_CGIStr
    
    set retcode [AP_Router::HttpGet_Chk $Func_INI::dut_lanip $Func_INI::dut_username_password]
    set txt "Retcode= $retcode"
    Misc::insertLogLine "info" $txt
    set txt  "After DUT Reset Duraiton= $Func_INI::dut_resettime seconds"
    Misc::insertLogLine "info" $txt
    after [expr $Func_INI::dut_resettime * 1000];update
    
    AP_Router::GetUpdatedCGIString $list_cgistringid $list_CGIStr
    set url [AP_Router::GetDUTURL_POST $Func_INI::dut_lanip $Func_INI::dut_username_password \
            $list_CGIStr $Func_INI::dut_cgistringid]
    
    set retcode [AP_Router::HttpPost_Auth $url $AP_Router::str_cgi_keyvalue]
    
    set txt "Waiting $Func_INI::dut_wakeuptime seconds to setup WAN mode"
    Misc::insertLogLine "info" $txt
    after [expr $Func_INI::dut_wakeuptime * 1000];update
    
    return true
}
proc AP_Router::log_shellfile {shellfile list_data} {
    ##Check file exists or not
    if [file exists $shellfile] {
        file delete -force $shellfile
        
    }
    
    set f [open [lindex $shellfile 0] {RDWR CREAT APPEND}] ;# instead of "a"
    fconfigure $f -encoding utf-8
    foreach {data} $list_data {
        puts $f $data
        puts $f "\n"
        # puts $f "sleep 5s"
    }
    close $f
    
}
proc AP_Router::Gen_Curl_Linux {shell_file list_str_url list_str_cgi_keyvalue} {
    set char_space    " "
    set char_quot    "\""
    set char_equal    "="
    set char_doallsign "$"
    
    for {set i 0} {$i < [llength $list_str_url]} {incr i} {
        set url [lindex $list_str_url $i]
        set str_cgi_keyvalue [lindex $list_str_cgi_keyvalue $i]
        if [info exist str_postdata] {
            unset str_postdata
        }
        if [info exist str_curl] {
            unset str_curl
        }
        append str_postdata "postdata$i" $char_equal $char_quot $str_cgi_keyvalue \
                $char_space $url $char_quot
        append str_curl "curl" $char_space "-m 20 -d" $char_space $char_doallsign "postdata$i"
        
        lappend list_postdata_curl $str_postdata $str_curl
        
    };#for {set i 0} {$i < [llength $list_str_url]} {incr i}
    
    log_shellfile $shell_file $list_postdata_curl
}
proc AP_Router::Gen_Curl_Win {shell_file list_str_url list_str_cgi_keyvalue} {
    set char_space    " "
    set char_quot    "\""
    set char_equal    "="
    set char_percent "%"
    set bat_cmd_set     "SET"
    set list_postdata_curl {}
    set dir [file dirname [pwd]]
    set path_lib [file join $dir "lib"]
    set path_curl [file join $path_lib "curl.exe"]
    set native_curl [file nativename $path_curl]
    
    lappend list_postdata_curl "@echo off"
    
    for {set i 0} {$i < [llength $list_str_url]} {incr i} {
        set url [lindex $list_str_url $i]
        set str_cgi_keyvalue [lindex $list_str_cgi_keyvalue $i]
        if [info exist str_postdata] {
            unset str_postdata
        }
        if [info exist str_url] {
            unset str_url
        }
        if [info exist str_curl] {
            unset str_curl
        }
        ################################################################################
        # output below strings to file "post_data.txt".
        # data = "..."
        # url = "..."
        ################################################################################
        append str_postdata "data" $char_space $char_equal $char_space $char_quot \
                $str_cgi_keyvalue $char_quot
        
        append str_url    "url" $char_space  $char_equal $char_space $char_quot \
                        $url $char_quot
        
        log_shellfile "post_data$i.txt" [list $str_postdata $str_url]
        ################################################################################
        # output below strings to file "post_cgi.bat".
        #
        # @echo off
        # 
        # curl -K post_data.txt -o curl.log
        # 
        # @echo on
        ################################################################################
        append str_curl    $native_curl " -K" $char_space "post_data$i.txt" " -o curl$i.log"
        
        lappend list_postdata_curl $str_curl
        
    };#for {set i 0} {$i < [llength $list_str_url]} {incr i}
    
    lappend list_postdata_curl "@echo on"
    
    log_shellfile $shell_file $list_postdata_curl
    
}
proc AP_Router::Exec_Curl {shell_file} {
    global tcl_platform
    
    if {"$tcl_platform(platform)" != "windows"} {
        catch {exec chmod 755 ./$shell_file} errmsg
        puts $errmsg
        catch {exec ./$shell_file} errmsg
    } else  {
        catch {exec $shell_file} errmsg
    }
    
    
    puts $errmsg
}
################################################################################
# Generate shell script for DUT setting by CDRouter Modules
################################################################################
proc AP_Router::DUT_Setup_CDRouter_Moudule {list_cgistringid list_CGIStr} {
    global tcl_platform
    
    set list_str_cgi_keyvalue [AP_Router::GetUpdatedCGIString_List $list_cgistringid $list_CGIStr]
    set list_str_url [AP_Router::GetDUTURL_POST_List $Func_INI::dut_lanip \
            $Func_INI::dut_username_password $list_CGIStr $list_cgistringid]
    
    
    if {"$tcl_platform(platform)" == "windows"} {
        set shell_file    "post_cgi.bat"
        AP_Router::Gen_Curl_Win $shell_file $list_str_url $list_str_cgi_keyvalue
    } else  {
        set shell_file    "post_cgi.sh"
        AP_Router::Gen_Curl_Linux $shell_file $list_str_url $list_str_cgi_keyvalue
    }
    
    AP_Router::Exec_Curl $shell_file
    
    set txt "DUT WakeUP Duraiton= $Func_INI::dut_wakeuptime seconds"
    Misc::insertLogLine "info" $txt
    after [expr $Func_INI::dut_wakeuptime * 1000];update
    
    return true
}
proc AP_Router::DUT_Setting {cdrouter_module list_CGIStr} {
    ##Setup WAN mode at PPPoE;PPTP;DHCP
    set list_cgistringid $Func_INI::dut_cgistringid
    AP_Router::DUT_WAN_Setting $list_cgistringid $list_CGIStr
    
    ##Setup DUT by CDRouter module
    switch -regexp [string tolower $cdrouter_module] {
        {^vservice} {
            ##Setup DUT vertual server 
            set list_cgistringid $Func_INI::dut_cgistringid_virtual_server
            AP_Router::DUT_Setup_CDRouter_Moudule $list_cgistringid $list_CGIStr
        }
        {^url-filter} {
            ##Setup DUT URL Filter
            
            set list_cgistringid $Func_INI::dut_cgistringid_url_filter
            AP_Router::DUT_Setup_CDRouter_Moudule $list_cgistringid $list_CGIStr
        }
        {^triggerp} {
            ##Setup DUT trigger port
            
            set list_cgistringid $Func_INI::dut_cgistringid_port_trigger
            AP_Router::DUT_Setup_CDRouter_Moudule $list_cgistringid $list_CGIStr
        }
        {^dmz} {
            ##Setup DUT DMZ
            
            set list_cgistringid $Func_INI::dut_cgistringid_dmz
            AP_Router::DUT_Setup_CDRouter_Moudule $list_cgistringid $list_CGIStr
        }
        {^apps} {
            set list_cgistringid $Func_INI::dut_cgistringid_apps
            AP_Router::DUT_Setup_CDRouter_Moudule $list_cgistringid $list_CGIStr
        }
        {^rip} {
            set list_cgistringid $Func_INI::dut_cgistringid_rip
            AP_Router::DUT_Setup_CDRouter_Moudule $list_cgistringid $list_CGIStr
        }
        {^dyndns} {
            set list_cgistringid $Func_INI::dut_cgistringid_dyndns
            AP_Router::DUT_Setup_CDRouter_Moudule $list_cgistringid $list_CGIStr
        }
        {^static} {
            set list_cgistringid $Func_INI::dut_cgistringid_staticroute
            AP_Router::DUT_Setup_CDRouter_Moudule $list_cgistringid $list_CGIStr
        }
    };#switch -regexp [string tolower $cdrouter_module]
    
    return true
}
################################################################################
# Refer demo sample code diff2.tcl
################################################################################
proc AP_Router::DiffList {lines1 lines2} {
    variable verbose
    variable logfile
    
    if {$verbose == "on"} {
        Log::LogList_level "info" $logfile [list $lines1 $lines2]
    }
    
    ::struct::list assign [::struct::list longestCommonSubsequence $lines1 $lines2] x1 x2
    
    foreach chunk [::struct::list lcsInvertMerge2 $x1 $x2 [llength $lines1] [llength $lines2]] {
        
        if {$verbose == "on"} {
            Log::LogList_level "info" $logfile [list "===========================================" \
                    $chunk "-------------------------------------------"]
        }
        
        ::struct::list assign [lindex $chunk 1] b1 e1
        ::struct::list assign [lindex $chunk 2] b2 e2
        switch -exact -- [lindex $chunk 0] {
            changed {
                if {$verbose == "on"} {
                    Log::LogList_level "info" $logfile [list "< [join [lrange $lines1 $b1 $e1]]" \
                            "---" "> [join [lrange $lines2 $b2 $e2]]"]
                    Log::LogList_level "info" $logfile [list "==========================================="]
                };#if {$verbose == "on"}
                
                ## return [lindex $chunk 0];#add Sep08-2006 by Philip
            }
            added   {
                if {$verbose == "on"} {
                    Log::LogList_level "info" $logfile [list "> [join [lrange $lines2 $b2 $e2]]" ]
                    Log::LogList_level "info" $logfile [list "==========================================="]
                };#if {$verbose == "on"}
                
                ## return [lindex $chunk 0];#add Sep27-2006 by Philip
            }
            deleted {
                if {$verbose == "on"} {
                    Log::LogList_level "info" $logfile [list "< [join [lrange $lines1 $b1 $e1]]" ]
                    Log::LogList_level "info" $logfile [list "==========================================="]
                };#if {$verbose == "on"}
                
                ## return [lindex $chunk 0];#add Sep27-2006 by Philip
            }
        };#switch -exact -- [lindex $chunk 0]
    };#foreach chunk [::struct::list lcsInvertMerge2 $x1 $x2 [llength $lines1] [llength $lines2]]
    
    if {$verbose == "on"} {
        Log::LogList_level "info" $logfile [list "==========================================="]
    };#if {$verbose == "on"}
    
    if {($b1==$b2) && ($e1==$e2)} {
        return [lindex $chunk 0];#return "unchanged"
    }
    return trues
}

;#>>>
namespace eval Func_CSV {
    package require csv
    # package require struct
    package require fileutil
    
    variable verbose off
    variable logfile "../log/csvfile.log"
    
    variable list_noisepathfname_ChNum {}
    variable str_noisepathfname_CO ""
    variable str_noisepathfname_CPE ""
    
    variable LoopLen
    variable BTLen
    variable TestProfile ""
    variable sepChar ","
    variable str_testplan_table ""
    variable LoopType ""
}
proc Func_CSV::Read {list_csvfname} {
    
    variable sepChar
    variable verbose
    variable logfile
    
    if {[llength $list_csvfname] == 0} {
        set list_csvfname -
    }
    
    set stdin 1
    set first 1
    if [info exists m] {m destroy }
    
    # struct::matrix m
    set list_csv {}
    
    foreach f $list_csvfname {
        if {![string compare $f -]} {
            if {!$stdin} {
                puts stderr "Cannot use - (stdin) more than once"
                exit -1
            }
            set in stdin; set stdin 0
        } else {
            set in [open $f r]
        };#if {![string compare $f -]}
        
        if {$first} {
            if {$verbose == on} {
                Log "info" $logfile [list "CSV File= $f"]
            }
            
            while {[eof $in] != 1} {
                if {[gets $in line] > 0} {
                    switch -regexp $line  {
                        {^(#|;)} {
                            continue
                        }
                        {([0-9]|[0-9][0-9]|[0-9][0-9][0-9]),} {
                            set list_data [::csv::split $line $sepChar]
                        }
                        {([0-9][0-9][0-9][0-9]),} {
                            set list_data [::csv::split $line $sepChar]
                        }
                        default {
                            set list_data [::csv::split $line $sepChar]
                        }
                    };#switch -regexp $linr
                    # m add columns [llength $list_data]
                    # m add row $list_data
                    lappend list_csv $list_data
                };#if {[gets $in line] > 0}
                
            };#while {[eof $in] != 1}
        };#if {$first}
        
        # csv::read2matrix $in m $sepChar
        
        if {[string compare $f -]} {
            close $in
        }
    };#foreach f $list_csvfname
    
    return $list_csv
}
proc Func_CSV::ReadCGIStringfromFile {list_row cgistr_id} {
    variable verbose
    variable targetid_cgi_str ""
    
    set prefix_path [file join [pwd] ".." "lib" "httppkts"]
    
    # {1000 Post post.cgi pppoe.txt PPPoE_setting_WRT100}
    #    0    1      2        3            4
    #####################################################
    foreach {element} $list_row {
        set id [lindex $element 0]
                
        if {$id == $cgistr_id} {
            set cgistr_fname [lindex $element 3]
            set path_cgistr_fname [file join $prefix_path $cgistr_fname]
            
            
            
            #Check path_cgistr_fname if available
            if {[file exists $path_cgistr_fname] == 1} {
                catch {fileutil::cat $path_cgistr_fname} ret_cgistring
                # puts "[string last "not" $ret_cgistring]"
                if {$verbose == "on"} {
                    puts "In proc Func_CSV::ReadCGIStringfromFile"
                    puts "element= $element; id= $id."
                    puts "cgistr_fname= $cgistr_fname"
                    puts "path_cgistr_fname= $path_cgistr_fname"
                    puts "ret_cgistring: $ret_cgistring"
                }
                
                set targetid_cgi_str $ret_cgistring
                return true
            } else  {
                return false
            };#if {[file exists $path_cgistr_fname] == 1}
            
        };#if {$id == $cgistr_id}
        
    };#foreach {element} $temp
   return false
}
proc Func_CSV::GetTargetCGIString {list_row cgistr_id} {
    variable verbose
    variable logfile
    
    set retval [Func_CSV::ReadCGIStringfromFile $list_row $cgistr_id]
    return $retval
}
proc Func_CSV::GetPostTarget {list_row list_cgistr_id} {
    variable verbose
    variable logfile
    
    # {1000 Post post.cgi pppoe.txt PPPoE_setting_WRT100}
    #    0    1      2        3            4
    #####################################################
    foreach {element} $list_row {
        set id [lindex $element 0]
        foreach {cgistr_id} $list_cgistr_id {
            if {$id == $cgistr_id} {
                set target [lindex $element 2]
                lappend list_target $target
            };#if {$id == $cgistr_id}
        }
    };#foreach {element} $temp
    
    return $list_target
}
proc Func_CSV::Log {level filename list_msg} {
    foreach temp $list_msg {
        set msg "$temp"
        Log::tofile $level $filename $msg
    }
}
namespace eval Misc {
}
proc Misc::insertLogLine {level txt} {
    append strmsg "\n" "\[[clock format [clock seconds] -format "%H:%M:%S"]\] " \
            "\[[string toupper $level]\] " $txt
    
    puts $strmsg
}
proc Misc::elapsed_time { seconds { resolution hours }} {
    
    if { "$resolution" == "minutes" } {
        
        set minutes [ expr (($seconds)/60) ]
        set seconds [ expr ( $seconds - ($minutes * 60)) ]
        
        return [ format "%2.2d:%2.2d" $minutes $seconds ]
        
    } else {
        
        set hours [ expr $seconds / 3600 ]
        set minutes [ expr (($seconds - ($hours * 3600))/60) ]
        set seconds [ expr ( $seconds - ($hours * 3600 + $minutes * 60)) ]
        
        return [ format "%2.2d:%2.2d:%2.2d" $hours $minutes $seconds ]
    }
}
proc Misc::report_duration {start_time finish_time} {
    set duration [ expr $finish_time - $start_time ]
    puts "\n--- end of CDRouter test ---"
    puts "Test run started at [ clock format $start_time ]"
    puts "Test run stopped at [ clock format $finish_time ], total time [ Misc::elapsed_time $duration ]"
    puts "\n"
}