# ChangeLog
# By Philip Shen @SWD Gemtek Corp.
# ---------------------------------------------------------------------
# Sep12-2007 Initialize API_CDRouter.tcl.
# Oct12-2007 In proc CDRouter::Run, add check cdrouter version of capture optioon.
################################################################################


########################################
# Define a simple custom logproc
##########################################
namespace eval Log {
    package require logger
}

# Define a simple custom logproc
proc Log::log_to_file {lvl logfiletxt} {
    # set logfile "mylog.log"
    set msg "\[[clock format [clock seconds] -format "%H:%M:%S %m%d%Y"]\] \
            [lindex $logfiletxt 1]"
    set f [open [lindex $logfiletxt 0] {WRONLY CREAT APPEND}] ;# instead of "a"
    fconfigure $f -encoding utf-8
    puts $f $msg
    close $f
}
################################################################################
#${log}::logproc level
# ${log}::logproc level command
# ${log}::logproc level argname body
# This command comes in three forms - the third, older one is deprecated
# and may be removed from future versions of the logger package.
# The current set version takes one argument, a command to be executed
# when the level is called. The callback command takes on argument,
# the text to be logged. If called only with a valid level logproc returns
# the name of the command currently registered as callback command.
# logproc specifies which command will perform the actual logging for a given level.
# The logger package ships with default commands for all log levels,
# but with logproc it is possible to replace them with custom code.
# This would let you send your logs over the network, to a database, or anything else.
################################################################################
proc Log::tofile {level strfilename strtext} {
    # Initialize the logger
    set log [logger::init global]
    
    # Install the logproc for all levels
    foreach lvl [logger::levels] {
        interp alias {} log_to_file_$lvl {} Log::log_to_file $lvl
        ${log}::logproc $lvl log_to_file_$lvl
    }
    
    ${log}::$level [list $strfilename $strtext]
    
    return true
}

proc Log::parray {a {pattern *}} {
    upvar 1 $a array
    if ![array exists array] {
        error "\"$a\" isn't an array"
    }
    set maxl 0
    foreach name [lsort [array names array $pattern]] {
        if {[string length $name] > $maxl} {
            set maxl [string length $name]
        }
    }
    set maxl [expr {$maxl + [string length $a] + 2}]
    foreach name [lsort [array names array $pattern]] {
        set nameString [format %s(%s) $a $name]
        puts stdout [format "%-*s = %s" $maxl $nameString $array($name)]
    }
}
proc Log::LogList_level {level filename list_msg} {
    set path_logfile [file join ".." "log" $filename]
    
    foreach temp $list_msg {
        set strmsg "$temp"
        Log::tofile $level $path_logfile $strmsg
    }
}
proc Log::LogOut {str_out logfname} {
    
    set path_logfile [file join ".." "log" $logfname]
    # set clk [clock format [clock seconds] -format "%b%d%H%M%S%Y"]
    # append str_out;# "  ";append Str_Out $clk
    
    set rnewinfd [open $path_logfile a]
    puts $rnewinfd $str_out
    flush $rnewinfd;close $rnewinfd
}

namespace eval CDRouter {
    variable test_name {{basic.tcl cdrouter_basic_} {dhcp-c.tcl cdrouter_dhcp_} \
                {pppoe-c.tcl cdrouter_pppoe_client_} {nat.tcl cdrouter_nat_} \
            {apps.tcl cdrouter_app_}}
}
proc CDRouter::Param_GetConfig {path configfname {cdrouter_module {}} } {
    
    ##while cdrouter module is firewall, to disable DMZ option
    switch -regexp [string tolower $cdrouter_module] {
        {^firewall} {
            set templist [split $configfname .]
            append firewall_configfname [lindex $templist 0] "_" "firewall" "." "conf"
            append path_config [file join $path $firewall_configfname]
        }
        default {
            append path_config [file join $path $configfname]
        }
    };#
    
    return $path_config
}
proc CDRouter::Param_GetTestPath {pathfname} {
    set prefix "\/usr\/share\/doc\/"
    append path_testpath $prefix $pathfname
    return $path_testpath
}
proc CDRouter::Param_GetLog {path dut_model dut_ver dut_wan_mode cdrouter_module} {
    set timestamp [clock format [clock seconds] -format "%H%M%S_%m%d%Y"]
    set logfname "$dut_model-$dut_ver-$dut_wan_mode-$cdrouter_module-$timestamp"
    set path_file_html [file join $path "$logfname.html"]
    set path_file_log [file join $path "$logfname.log"]
    
    append fold_capture $dut_model "_" $cdrouter_module "_" $timestamp
    set path_capture [file join $path $fold_capture]
    
    set list_path_log [list $path_file_html $path_file_log $path_capture]
    return $list_path_log
}
proc CDRouter::Param_GetTestName {module} {
    variable test_name 
    
    foreach {pair} $test_name {
        set pair_module [lindex $pair 0]
        set pair_testname [lindex $pair 1]
        # puts "$pair_module; $pair_testname"
        if {$pair_module == $module} {
            return $pair_testname
        }
    }
    
    return false
}
proc CDRouter::Param_Buddy {module dut_model dut_fwver cdrouter_cofig cdrouter_testpath} {
    set str_module "-module $module"
    # puts $str_module
    
    set log_path [file join [pwd] ".." "log"]
    set str_log [CDRouter::Param_GetLog $log_path $dut_model $dut_fwver \
            [lindex [split $module .] 0]]
    # puts $str_log
    
    set doc_path [file join [pwd] ".." "doc"]
    set str_config [CDRouter::Param_GetConfig $doc_path $cdrouter_cofig]
    # puts $str_config
    set str_testpath [CDRouter::Param_GetTestPath $cdrouter_testpath]
    # puts $str_testpath
    
    append buddy_param $str_module " " $str_testpath \
            " " $str_config " " "-trace" " " $str_log
            
    return $buddy_param
}
proc CDRouter::Show_ModuleLogHtml {dut_model dut_fwver dut_wan_mode cdrouter_module} {
    set txt "Module=$cdrouter_module"
    Misc::insertLogLine "info" $txt
    
    set log_path [file join [file dirname [pwd]] "log"]
    
    set list_path_log [CDRouter::Param_GetLog $log_path $dut_model $dut_fwver \
            $dut_wan_mode [lindex [split $cdrouter_module .] 0]]
    set html [lindex $list_path_log 0]
    set log [lindex $list_path_log 1]
    set log_capture_path [lindex $list_path_log 2]
    
    set txt "path_html= $html"
    Misc::insertLogLine "info" $txt
    set txt "path_log= $log"
    Misc::insertLogLine "info" $txt
    set txt "path_capture= $log_capture_path"
    Misc::insertLogLine "info" $txt
    
    set ret_list [list $html $log $log_capture_path]
    return $ret_list
}
proc CDRouter::Run {dut_model dut_fwver dut_wan_mode cdrouter_cofig cdrouter_testpath list_CGIStr} {
    set doc_path [file join [file dirname [pwd]] "doc"]
    
    set path_testpath [CDRouter::Param_GetTestPath $cdrouter_testpath]
    set txt "path_testpath= $path_testpath"
    Misc::insertLogLine "info" $txt
    
    foreach {tt1} $Func_INI::cdrouter_testname_id {
        foreach {element} $tt1 {
            foreach {module list_testnameid} $element {
                ##While do firewall.tcl test, using PPPoE_firewall.conf file to disable DMZ option
                set path_config [CDRouter::Param_GetConfig $doc_path $cdrouter_cofig $module]
                set txt "path_config= $path_config"
                Misc::insertLogLine "info" $txt
                
                if {$list_testnameid == 0} {
                    ##Setup DUT by CDRouter module by each test module
                    AP_Router::DUT_Setting $module $list_CGIStr
                    
                    set retlist [CDRouter::Show_ModuleLogHtml $dut_model $dut_fwver $dut_wan_mode $module]
                    set html [lindex $retlist 0]
                    set log [lindex $retlist 1]
                    set log_capture_path [lindex $retlist 2]
                    
                    if {$Func_INI::cdrouter_ver < 3} {
                        #For CDRouter verison == 2.7
                        catch {exec buddy -config $path_config -testpath $path_testpath -module $module -color -report-html $html | tee $log} screendump
                    } else  {
                        #For CDRouter verison > 2.7
                        catch {exec buddy -capture -logdir $log_capture_path -config $path_config -testpath $path_testpath -module $module -color -report-html $html | tee $log} screendump
                    };#if {$Func_INI::cdrouter_ver < 3}
                    
                } else {
                    
                    foreach {testnameid} $list_testnameid {
                        ##Setup DUT by CDRouter module by each testcase
                        AP_Router::DUT_Setting $module $list_CGIStr
                        
                        set test_name [CDRouter::Param_GetTestName $module]
                        set testcase_id "$test_name$testnameid"
                        set txt  "TestNameID= $testcase_id"
                        Misc::insertLogLine "info" $txt
                        
                        set retlist [CDRouter::Show_ModuleLogHtml $dut_model $dut_fwver $dut_wan_mode $testcase_id]
                        set html [lindex $retlist 0]
                        set log [lindex $retlist 1]
                        
                        catch {exec buddy -config $path_config -testpath $path_testpath -execute $testcase_id -color -report-html $html | tee $log} screendump
                    };#foreach {testnameid} $list_testnameid
                    
                };#if {$list_testnameid == 0}
                
                puts $screendump;update
                
            };#foreach {module list_testnameid} $element
            
        };#foreach {element} $tt1
        
    };#foreach {tt1} $Func_INI::cdrouter_testname_id
    
    return true
}
;#>>>
namespace eval API_TclCurl {
    package require TclCurl
    
    variable verbose "off"
    variable logfile "../log/TclCurl.log"
}

proc API_TclCurl::http_get {url args} {
    set pairs {}
    foreach {name value} $args {
        lappend pairs "[curl::escape $name]=[curl::escape $value]"
    }
    append url ? [join $pairs &]
    
    set curlHandle [curl::init]
    $curlHandle configure -url $url -bodyvar html
    catch { $curlHandle perform } curlErrorNumber
    if { $curlErrorNumber != 0 } {
        error [curl::easystrerror $curlErrorNumber]
    }
    $curlHandle cleanup
    
    return $html
}

proc API_TclCurl::http_post {url args} {
    set pairs {}
    foreach {name value} $args {
        lappend pairs "[curl::escape $name]=[curl::escape $value]"
    }
    set data [join $pairs &]
    
    set curlHandle [curl::init]
    $curlHandle configure -url $url -bodyvar html -post 1 -postfields $data
    catch { $curlHandle perform } curlErrorNumber
    if { $curlErrorNumber != 0 } {
        error [curl::easystrerror $curlErrorNumber]
    }
    $curlHandle cleanup
    
    return $html
}
